#!/bin/env node
 
var express = require('express');
var mongoose = require('mongoose');
var User = require(__dirname+'/model/users.js');
var app = express();

//  Get the environment variables we need.
var port = process.env.PORT || 3000;

// Establish connection to MongoDB
mongoose.connect('mongodb://localhost/nodetest');
//mongoose.connect('mongodb://aprasad1993@gmail.com:airtelairtel@oceanic.mongohq.com:10053/app24853216');
var passport = require('passport')
require('./config/passport')(passport,User)

app.configure(function () {
    app.use(express.static('public'));
    app.use(express.cookieParser());
    app.use(express.bodyParser());
    //app.use(express.session({ secret: 'keyboard cat' }));
    app.use(passport.initialize());
    //app.use(passport.session());
    app.use(app.router);
    app.use(express.methodOverride());
});

// set up the RESTful API, handler methods are defined in newproject.js
var newproject = require('./controller/newproject.js');
var rental = require('./controller/rental.js');
var user = require('./controller/user.js');
var contact_us = require('./controller/contact_us.js');

//user routes
app.post('/login',passport.authenticate('local'), user.session)
app.post('/user', user.create_user);	
app.post('/contact',contact_us.addMessage);

/*new project route*/
app.post('/newproject/:city',newproject.create_project);
app.get('/newproject/:city/:name', newproject.get_project);
app.get('/newproject/full/:city/:id', newproject.get_complete_project);
app.put('/newproject/:city/:id',newproject.update_project);
app.del('/newproject/:city/:id',newproject.delete_project);

/*rental routes*/
app.post('/rent/:city',rental.create_rental);
app.get('/rent/:city',rental.get_all_rental)
app.get('/rent/:city/:id', rental.get_rental);
app.put('/rent/:city/:id',rental.update_rental);
app.del('/rent/:city/:id',rental.delete_rental);

//The 404 Route (ALWAYS Keep this as the last route)
app.get('*', function(req, res){
  res.send('what???', 404);
});
//  And start the app on that interface (and port).
app.listen(port, function() {
   console.log('%s: Node server started on %s:%d ...', Date(Date.now() ), port);
});

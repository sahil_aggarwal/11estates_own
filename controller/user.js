/* user model for login*/
var User = require('../model/users.js');

/*route if user successfully login*/
exports.session = function (req, res) {
  res.send("login request received");
}

exports.create_user = function(req,res)
{
	User.findOne({username: req.body.username}).exec(function(err,user){
		if(err)
		res.send('following error occured: '+err);
		else
		{
			if(user)
			res.send('username already exists ');
			else
			{
				var user = new User({username:req.body.username,password:req.body.password});
				user.save(function (err) {
				if (err) throw err;
				res.send('user saved.');
				});
			}
		}
		
	});
}
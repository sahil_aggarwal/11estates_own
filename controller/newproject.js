var mongoose = require('mongoose'); 
var NewProjectSchema = require('../model/new_project.js');

/*different collections with same schema for different cities*/ 
var newProjectNoida = mongoose.model('NewProjectNoida', NewProjectSchema);
var newProjectGurgaon = mongoose.model('NewProjectGurgaon', NewProjectSchema);
var newProjectDelhi = mongoose.model('NewProjectDelhi', NewProjectSchema);
var newProjectKolkata = mongoose.model('NewProjectKolkata', NewProjectSchema);
var newProjectGaziabad = mongoose.model('NewProjectGaziabad', NewProjectSchema);

/* Amazon bucket for uploading and retrieving data*/
var fs = require('fs');
var AWS = require('../AWS.js');
var s3 = new AWS.S3({params: {Bucket: '11states'}});

/*select model according to selected city*/
function select_city_model(city)
{
	if(city.toUpperCase().localeCompare('NOIDA') === 0)
	{
		return newProjectNoida;
	}
	else if(city.toUpperCase().localeCompare('GURGAON') === 0)
	{
		return newProjectGurgaon;
	}
	else if(city.toUpperCase().localeCompare('DELHI') === 0)
	{
		return newProjectDelhi;
	}
	else if(city.toUpperCase().localeCompare('KOLKATA') === 0)
	{
		return newProjectKolkata;
	}
	else if(city.toUpperCase().localeCompare('GAZIABAD') === 0)
	{
		return newProjectGaziabad;
	}
	else 
	{
		return null;
	}
} 
/*upload image with specified path to s3 bucket*/
function upload_image_to_bucket(path,name)
{
	fs.readFile(path, function(err, file_buffer){
	if(!err)
	{
		var params = {
		Key: name,
		Body: file_buffer
		};
		s3.putObject(params, function (perr, pres) {
			if (perr) {
				console.log("Error uploading data: ", perr);
			} else {
				console.log("Successfully uploaded data to myBucket/myKey");
			}
		});
	}
	else
	  console.log(err);
	});
	return name;
}
/*upload cover pic and images  */
function handle_images(images,name)
{
	
	var path = images[0].path;
	
	cover_pic = upload_image_to_bucket(path,'cover_pic_'+name);
	
	for(var i = 1;i < req.files.image.length; i++)
	{
		unique_name = new Date();
		path = image[i].path;
		images.push(upload_image_to_bucket(path,''+name+i));
	}
	return {
			 images:images,
			 cover_pic:cover_pic
		   };
}


/*set config properties(price_min, price_max, size_min, size_max) */
function set_property_config(config)
{

	var price_min = 10000000000000;
	var price_max = 0;
	var size_min = 10000000000000;
	var size_max = 0;
	var bhk = [];
	if(config)
	{
		for(var i = 0;i < config.length; i++)
		{
			if(config[i].price < price_min)
			{
				price_min = config[i].price;
			}
			if(config[i].price > price_max)
			{
				price_max = config[i].price;
			}
			if(config[i].price < size_min)
			{
				size_min = config[i].area;
			}
			if(config[i].price > size_max)
			{
				size_max = config[i].area;
			}
			if(bhk.indexOf(config[i].bhk) == -1)
			bhk.push(config[i].bhk);
		}
	}
	return {price_min:price_min,price_max:price_max,size_min:size_min,size_max:size_max,bhk:bhk};
}
function set_project(req)
{
	var project = {};
	var config = req.body.propertyConfigurations || null;
	var cover_pic = null;
	var images = [];
	var returnData;
	var configRange;
	if(req.files && req.files.image && req.files.image.length > 0)
	{
		returnData = handle_images(req.files.images,req.body.name);
		cover_pic = returnData.cover_pic;
		images = returnData.images;
	}
	
	configRange = set_property_config(config);
	project.description = req.body.projectDescription;
	project.name = req.body.name;
	project.address = req.body.address;
	project.longitude = req.body.location.longitude;
	project.latitude = req.body.location.latitude;
	project.amenities = req.body.amenities;
	project.configurations = config;
	project.bhk_num = configRange.bhk;
	project.price_min = configRange.price_min;
	project.price_max = configRange.price_max;
	project.size_min = configRange.size_min;
	project.size_max = configRange.size_max;
	project.cover_pic = cover_pic;
	project.images = images;
	return project;
}

/*create a new project with given configuration*/
exports.create_project = function(req, res){

	var NewProject;
	if(req.params.city)
	{
		NewProject = select_city_model(req.params.city);
		
		if(NewProject !== null && req.body.projectName )
		{
			NewProject.findOne({name: req.body.projectName}).exec(function(err,project)
			{
				if(project)
				res.send('Project name already exists');
				else
				{
					var project = set_project(req);
					var new_project = 	new NewProject(project);
					
					new_project.save(function (err) {
						if (err) throw err;
						res.send('new_project saved.');
					});
					var x;
				}
				
			})
			
		}
		else
		{
			res.send("city not recognized: city: "+req.param.city +" or project name not defined");
		}
		
	}
	else
	{
		res.send("city params not defined");
	}
	
		
}

exports.get_project = function(req,res)
{
	var NewProject;
	if(req.params.city)
	{
	    NewProject = select_city_model(req.params.city);
		if(NewProject !== null)
		{
			var name = req.params.name;
			NewProject.find({name:name}).exec(function(err,project){
				if(err)
				res.send(err);
				else
				{
					if(project.length == 0)
					res.send("No project exist with given name");
					else if(project.length>1)
					res.send("Multiple project exist with same name");
					else
					{
						res.send({
							cover_pic: project[0].cover_pic||'',
							price_min: project[0].price_min,
							price_max: project[0].price_max,
							name: project[0].name,
							latitude:project[0].latitude,
							longitude:project[0].longitude,
							address:project[0].address,
							property_id: project[0]._id,
							bhk_min: Math.min.apply(null,project[0].bhk_num),
							bhk_max: Math.max.apply(null,project[0].bhk_num)
						});
					}
				}
			});
		}
		else
		{
			res.send("city not recognized: city: "+req.param.city);
		}
	}
	else
	{
		res.send("city params not defined");
	}
	
}

exports.get_complete_project = function(req,res)
{
	var NewProject;
	if(req.params.city)
	{
		NewProject = select_city_model(req.params.city);
		if(NewProject !== null)
		{
			var id = req.params.id;
			NewProject.findOne({_id:id}).exec(function(err,project){
				if(err)
				res.send(err);
				else
				{
					if(project === null)
					res.send("No project exist with given id");
					else
					{
						res.send(project);
					}
				}
			});	
		}
		else
		{
			res.send("city not recognized: city: "+req.param.city);
		}
	}
	else
	{
		res.send("city params not defined");
	}	
}
/*update project*/
exports.update_project = function(req,res)
{
	var NewProject;
	if(req.params.city)
	{
		NewProject = select_city_model(req.params.city);
		NewProject.findById(req.params.id,function(err, project){
			if(err)
			res.send(err);
			
			var new_project = set_project(req);
			project.description = new_project.projectDescription;
			project.name = new_project.name;
			project.address = new_project.address;
			project.longitude = new_project.location.longitude;
			project.latitude = new_project.location.latitude;
			project.amenities = new_project.amenities;
			project.configurations = new_project.configurations;
			project.bhk_num = new_project.bhk_num;
			project.price_min = new_project.price_min;
			project.price_max = new_project.price_max;
			project.size_min = new_project.size_min;
			project.size_max = new_project.size_max;
			project.cover_pic = new_project.cover_pic;
			project.images = new_project.images;
			// save the project
			project.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'Project updated!' });
			});
		});
	}	
}

/*delete project*/
exports.delete_project = function(req,res)
{
	var NewProject;
	if(req.params.city)
	{
		NewProject = select_city_model(req.params.city);
		NewProject.remove({
			_id: req.params.id
		}, function(err, project) {
			if (err)
				res.send(err);

			res.json({ message: 'Successfully deleted' });
		});
		
	}
	else
	res.send('city not defined');
}

/*

exports.retrieveImage = function(req,res)
{
	s3.listBuckets(function(err, data) {
   for (var index in data.Buckets) {
    var bucket = data.Buckets[index];
    console.log("Bucket: ", bucket.Name, ' : ', bucket.CreationDate);
   }
   });
}	*/
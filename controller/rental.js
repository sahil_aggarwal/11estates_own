var mongoose = require('mongoose'); 
var RentalSchema = require('../model/rental.js');

/*different collections with same schema for different cities*/ 
var rentalNoida = mongoose.model('RentalNoida', RentalSchema);
var rentalGurgaon = mongoose.model('RentalGurgaon', RentalSchema);
var rentalDelhi = mongoose.model('RentalDelhi', RentalSchema);
var rentalKolkata = mongoose.model('RentalKolkata', RentalSchema);
var rentalGaziabad = mongoose.model('RentalGaziabad', RentalSchema);

/* Amazon bucket for uploading and retrieving data*/
var fs = require('fs');
var AWS = require('../AWS.js');
var s3 = new AWS.S3({params: {Bucket: '11states'}});

/*select model according to selected city*/
function select_city_model(city)
{
	if(city.toUpperCase().localeCompare('NOIDA') === 0)
	{
		return rentalNoida;
	}
	else if(city.toUpperCase().localeCompare('GURGAON') === 0)
	{
		return rentalGurgaon;
	}
	else if(city.toUpperCase().localeCompare('DELHI') === 0)
	{
		return rentalDelhi;
	}
	else if(city.toUpperCase().localeCompare('KOLKATA') === 0)
	{
		return rentalKolkata;
	}
	else if(city.toUpperCase().localeCompare('GAZIABAD') === 0)
	{
		return rentalGaziabad;
	}
	else 
	{
		return null;
	}
} 

function upload_images(path,name,rental,res)
{
	if(typeof upload_images.counter == 'undefined')
	upload_images.counter = 0;
	
	
	fs.readFile(path, function(err, file_buffer){
	if(!err)
	{
		var params = {
						Key: name,
						Body: file_buffer
					};
		s3.putObject(params, function (perr, pres) {
		if (perr) {
			res.send("Error uploading data: ", perr);
		} else 
		{
			upload_images.counter++;
			if(upload_images.counter === upload_images.numImages)
			{
				upload_images.counter = 0;
			   rental.save(function(err){
				if(err)
				console.log(err);
				else
				res.send('rental saved');
			   });
			}
			else
			console.log('Processing...'+upload_images.counter+' uploaded');
			
		}
	    });
	}
	else
	{
		console.log(err);
	}
 }); 
} 

function set_rental(req)
{
	var setRental = {};
	//var cover_pic;
	//var images = [];
	
	//setRental.cover_pic = cover_pic;
	//setRental.images = images;
	setRental.street = req.body.street;
	setRental.info = req.body.info;
	setRental.rent = req.body.rent;
	setRental.longitude = req.body.longitude;
	setRental.latitude = req.body.latitude;
	setRental.size = req.body.size;
	setRental.bhk = req.body.bhk;
	setRental.bathrooms = req.body.bathrooms;
	setRental.amenities = req.body.amenities;
	setRental.security_deposite = req.body.security_deposite;
	return setRental;
}
/*delete from bucket*/
function delete_from_bucket(key)
{
	var params = {
				  Bucket: '11states', // required
				  Delete: { // required
					Objects: [ // required
					  {
						Key: key, // required
						
					  },
					  
					],
					Quiet: true || false,
				  },
				  
				};
					
				s3.deleteObjects(params, function(err, data) {
				  if (err) console.log(err, err.stack); // an error occurred
				  else     console.log(data);           // successful response
				});
}

exports.create_rental = function(req, res){
    
	var Rental;
	if(req.params.city)
	{
		Rental = select_city_model(req.params.city);
		if(Rental !== null )
		{
			var setRental = set_rental(req);
			var rental = 	new Rental(setRental);
			var name = ''+req.body.latitude+req.body.longitude;
			upload_images.numImages = (req.body.cover_pic?1:0) + (req.body.images?req.body.images.length:0);
			if(req.body.cover_pic)
			upload_images(req.body.cover_pic,'cover_pic_'+name,rental,res);
			
			if(req.body.images)
			for(var i = 0;i<req.body.images.length; i++)
			{
				upload_images(req.body.images[i],name+'_'+i,rental,res);
			}
			
	    }
        else
		res.send('city name not found');
	}
	else
	res.send('city param not defined')
	
}
/*get rental*/
exports.get_rental = function(req,res)
{
	var Rental;
	if(req.params.city)
	{
		Rental = select_city_model(req.params.city);
		if(Rental !== null)
		{
			var id = req.params.id;
			Rental.findOne({_id:id}).exec(function(err,rental){
				if(err)
				res.send(err);
				else
				{
					if(rental === null)
					res.send("No rental exist with given id");
					else
					{
						res.send(rental);
					}
				}
			});	
		}
		else
		{
			res.send("city not recognized: city: "+req.param.city);
		}
	}
	else
	{
		res.send("city params not defined");
	}	
}
/*update project*/
exports.update_rental = function(req,res)
{
	var Rental;
	if(req.params.city)
	{
		Rental = select_city_model(req.params.city);
		Rental.findById(req.params.id,function(err, rental){
			if(err)
			res.send(err);
			else if(rental)
			{
				//var imageNumber = parseInt(rental.images.split('_')[1]);
				//var deleteImages = req.body.deleteImages;
				//var addImages = req.files.images;
				rental.street = req.body.street;
				rental.info = req.body.info;
				rental.rent = req.body.rent;
				rental.longitude = req.body.longitude;
				rental.latitude = req.body.latitude;
				rental.size = req.body.size;
				rental.bhk = req.body.bhk;
				rental.bathrooms = req.body.bathrooms;
				/*if(deleteImages || Array.isArray(deleteImages) || deleteImages.length !=0)
				{
						for(var i = 0; i<deleteImages.length; i++ )
						{ 
						 delete_from_bucket(deleteImages[i]);
						 rental.images.pop(deleteImages[i]);
						}	
				}
				
				if(addImages || Array.isArray(addImages) || addImages.length !=0)
				{
						for(var i = 1; i<addImages.length; i++ )
						{
						 imageNumber += 1;	
						 upload_image_to_bucket(addImages[i],''+render.latitude+render.longitude+'_'+imageNumber);
						 rental.images.push(addImages[i])
						}	
				}
				
				if(req.body.cover_pic !== null)
				{
					delete_from_bucket(rental.cover_pic);
					upload_image_to_bucket(addImages[0],'cover_pic_'+render.latitude+render.longitude);
				}*/
				// save the project
				rental.save(function(err) {
					if (err)
						res.send(err);

					res.json({ message: 'Rental updated!' });
				});
			}	
		});
	}	
}

/*delete project*/
exports.delete_rental = function(req,res)
{
	var Rental;
	if(req.params.city)
	{
		Rental = select_city_model(req.params.city);
		/*remove data from bucket*/
		Rental.findById(req.params.id,function(err,rental){
			if(err)
			res.send(err);
			else
			{
				if(rental.cover_pic)
				delete_from_bucket(rental.cover_pic)
				
				for(var i = 0; i<rental.images.length; i++ )
				delete_from_bucket(rental.images[i])
				
				rental.remove();
				res.send("Successfully Deleted");
			}
			
		});
		
		
	}
	else
	res.send('city not defined');
}

exports.get_all_rental = function(req,res)
{
	var Rental;
	if(req.params.city)
	{
		Rental = select_city_model(req.params.city);
	}
	Rental.find({}).exec(function(err,rental){
		if(err)
		res.send(err);
		else
		res.send(rental);
	})	
	
}

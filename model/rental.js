var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

  
var RentalSchema = new Schema({
	street:{type : String, default : '', trim : true},
	info:{type : String, default : '', trim : true},
	rent:{type : Number, default : 0},
	size:{type : Number, default : 0},
	bhk:{type : Number, default : 0},
	bathrooms:{type : Number, default: 0},
	security_deposite:{type : Number, default : 0},
	latitude:{type : Number, default : 0},
	longitude:{type : Number, default : 0},
	amenities: [],
    cover_pic:{type : String, default : 'default', trim : true},
	images:[]	
});  
  
module.exports = RentalSchema;
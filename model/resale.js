var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

  
var resaleSchema = new Schema({
	street:String,
	info:String,
	price:Number,
	size:String,
	bhk:Number,
	bathrooms:Number,
	latitude:Number,
	longitude:Number,
	amenities: []		 
});  
  
module.exports = mongoose.model('Resale', resaleSchema);
var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

var ContactUsSchema = new Schema({
	name:String,
	email:String,
	mobile_num:Number,
	subject:String,
	message:String
});
module.exports = mongoose.model('Message', ContactUsSchema);
  
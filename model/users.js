var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
var crypto = require('crypto')
var UserSchema = new Schema({
	username:String,
	hashed_password:String,
	salt:String
});

UserSchema
  .virtual('password')
  .set(function(password) {
    this._password = password
    this.salt = this.makeSalt()
    this.hashed_password = this.encryptPassword(password)
  })
  .get(function() { return this._password })

var validatePresenceOf = function (value) {
  return value && value.length
}  
 

 
 UserSchema.pre('save', function(next) {
  if (!this.isNew) return next()

  if (!validatePresenceOf(this.password))
    next(new Error('Invalid password'))
  else
    next()
}) 
  
  UserSchema.methods = {
	makeSalt: function () {
    return Math.round((new Date().valueOf() * Math.random())) + ''
    },
	
	authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.hashed_password
    },
	
	encryptPassword: function (password) {
    if (!password) return ''
    var encrypred
    try {
      encrypred = crypto.createHmac('sha1', this.salt).update(password).digest('hex')
      return encrypred
    } catch (err) {
      return ''
    }
  }
  
}	
module.exports = mongoose.model('user', UserSchema);
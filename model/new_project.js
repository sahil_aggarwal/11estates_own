var mongoose = require('mongoose')
  , Schema = mongoose.Schema;

  
var NewProjectSchema = new Schema({
	description:String,
	name:String,
	address:String,
	configurations:[{
		bhk:Number,
		price:Number,
		area:Number,
		possession: Date,
	}],
	bhk_num:[{type:Number}],
	price_min:{type:Number, default:1000000},
	price_max:{type:Number, default:0},
	size_min:{type:Number, default:1000000},
	size_max:{type:Number, default:0},
	latitude:Number,
	longitude:Number,
	amenities: [],
    cover_pic:String,
	images:[]
    	
});  
  
module.exports =  NewProjectSchema;